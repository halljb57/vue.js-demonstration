<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('edit/{id}', function ($id) {
        $example = App\Example::with(['comments', 'tags'])->find($id);
        //return response()->json($example);
        return view('test', compact(['example']));
    });

    Route::get('api/examples', function () {
        $example = App\Example::with(['comments', 'tags'])->paginate(10);
        return response()->json($example);
    });

    Route::auth();

    Route::get('/home', 'HomeController@index');

    Route::get('/demo', function() {
        $example = App\Example::with(['comments', 'tags'])->paginate(10);
        $pageCount = $example->total()/10;
        return view('demo', compact(['examples', 'pageCount']));
    });
});
