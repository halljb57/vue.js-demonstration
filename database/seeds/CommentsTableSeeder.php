<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->delete();
        $faker = Faker::create('en_US');

        foreach(range(1,400) as $index){
            App\Comment::create([
                'markdown' => $faker->paragraph,
                'username' => App\User::all()->random()->name,
                'example_id' => App\Example::all()->random()->id,
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => $faker->dateTimeThisYear
            ]);
        }
    }
}
